#
# .zshrc
#

# ================================================================================
# ignores all other configurations if this script is running on special nodes

special_nodes=(slogin1 slogin2)
if [[ ${special_nodes[(r)b]} == $(hostname) ]]; then
    exit 0
fi


# ================================================================================
# configures $PATH

export PATH=$HOME/local/homebrew/bin:$PATH


# ================================================================================
# activates zplug and plugins

export ZPLUG_HOME=$HOME/local/homebrew/opt/zplug

mkdir -p $ZPLUG_HOME/log
source $ZPLUG_HOME/init.zsh

zplug "S1cK94/minimal", as:plugin, hook-build:"chmod +x *.zsh*", use:"*.zsh*"

zplug "plugins/common-aliases", from:oh-my-zsh
zplug "plugins/docker-compose", from:oh-my-zsh, if:"which docker-compose"
zplug "plugins/docker", from:oh-my-zsh, if:"which docker"
zplug "plugins/extract", from:oh-my-zsh
zplug "plugins/git", from:oh-my-zsh
zplug "plugins/nyan", from:oh-my-zsh
zplug "plugins/osx", from:oh-my-zsh, if:"[[ $OSTYPE == *darwin* ]]"
zplug "plugins/pip", from:oh-my-zsh
zplug "plugins/python", from:oh-my-zsh
zplug "plugins/rsync", from:oh-my-zsh
zplug "plugins/screen", from:oh-my-zsh
zplug "plugins/vagrant", from:oh-my-zsh

plugin_root="$HOME/local/dotfiles/dotfiles-core/zsh-plugins"
zplug $plugin_root, from:local, as:plugin, use:"direnv.sh", if:"which direnv"
zplug $plugin_root, from:local, as:plugin, use:"editor.sh", if:"which micro"
zplug $plugin_root, from:local, as:plugin, use:"git.sh"
zplug $plugin_root, from:local, as:plugin, use:"peco.sh", if:"which peco"
zplug $plugin_root, from:local, as:plugin, use:"python.sh", if:"which python || which python2 || which python3"
zplug $plugin_root, from:local, as:plugin, use:"rm.sh"
zplug $plugin_root, from:local, as:plugin, use:"ssh.sh"

if [ -f $HOME/.zshrc.local ]; then
    source $HOME/.zshrc.local
fi

zplug load
