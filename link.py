#!/usr/bin/env python

from __future__ import print_function

import argparse
import os


def main():
    #
    parser = argparse.ArgumentParser()
    parser.add_argument('--execute', action='store_true')
    args = parser.parse_args()

    #
    source_root_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'home')
    dest_root_path = os.path.expanduser('~')

    if args.execute:
        make_directory = lambda p: os.makedirs(p)
        make_link = lambda s, d: os.symlink(s, d)
        delete_file = lambda p: os.remove(p)
    else:
        make_directory = lambda p: print('mkdir -p {}'.format(p))
        make_link = lambda s, d: print('ln -s {} {}'.format(s, d))
        delete_file = lambda p: print('rm {}'.format(p))

    #
    for root, dirs, files in os.walk(source_root_path):
        for file_name in files:
            source_path = os.path.join(root, file_name)
            dest_path = os.path.join(
                dest_root_path,
                os.path.relpath(source_path, source_root_path))

            if os.path.exists(dest_path):
                if os.path.islink(dest_path) and os.readlink(dest_path) == source_path:
                        continue

                delete_file(dest_path)

            else:
                if not os.path.exists(os.path.dirname(dest_path)):
                    make_directory(os.path.dirname(dest_path))

            make_link(source_path, dest_path)


if __name__ == '__main__':
    main()
