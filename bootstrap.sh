#!/bin/bash

HOMEBREW_VERSION=1.1.5

# prepares ~/local
mkdir -p $HOME/local
mkdir -p $HOME/local/tmp

cd $HOME/local

# installs homebrew
if [ -d /Volumes ]; then
    curl -LO https://github.com/Homebrew/brew/archive/${HOMEBREW_VERSION}.zip
else
    curl -LO https://github.com/Linuxbrew/brew/archive/${HOMEBREW_VERSION}.zip
fi

unzip ${HOMEBREW_VERSION}.zip
mv brew-${HOMEBREW_VERSION} homebrew
rm ${HOMEBREW_VERSION}.zip

# sets up homebrew and installs some basic packages
export HOMEBREW_TEMP=$HOME/local/tmp
$HOME/local/homebrew/bin/brew install direnv micro peco pypy pypy3 python
