FROM centos:7

RUN yum install -y bzip2 gcc git make ruby unzip which zsh\
    && rm -rf /var/lib/yum/cache\
    && adduser east301

USER east301
RUN mkdir /home/east301/local

WORKDIR /home/east301/local
CMD ["/bin/bash"]
